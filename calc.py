import pandas as pd
import numpy as np

from geometry import angle, delta


def get_angles_between_positions(df: pd.DataFrame, sensor_position_1: str, sensor_position_2: str) -> pd.DataFrame:
    q1 = df[sensor_position_1]['q9'].values
    q2 = df[sensor_position_2]['q9'].values
    angles = np.rad2deg(np.pi - angle(delta(q1, q2)))
    result = pd.DataFrame(angles, columns=['Angle'])
    result['timestamp'] = df['timestamp']
    result['Time'] = (df['timestamp'].values - df['timestamp'].values[0]) / 1000.0
    return result
