from typing import Tuple, List, Union

import pandas as pd

__SENSORS = [
    'right_shoulder',
    'right_forearm',
    'right_wrist',
    'left_shoulder',
    'left_forearm',
    'left_wrist',
    'breast',
    'middle',
    'right_thigh',
    'right_shin',
    'right_foot',
    'left_thigh',
    'left_shin',
    'left_foot'
]


def __column_name_to_tuple(column: str) -> Tuple:
    tokens = column.split('_')
    if len(tokens) > 1:
        src_name = tokens[0]
        tokens = tokens[1].split('.')
        if len(tokens) > 1:
            sensor_id = int(tokens[0])
            axis = tokens[1]
            return __SENSORS[sensor_id], src_name, axis
    return column,


def index_to_multi_index(columns: pd.Index) -> pd.MultiIndex:
    return pd.MultiIndex.from_tuples([__column_name_to_tuple(c) for c in columns.values])


def load_data(filepath: str) -> pd.DataFrame:
    result = pd.read_csv(filepath)
    result.columns = index_to_multi_index(result.columns)
    return result
