# Steps of thinking

1. Decide how to organize parsed data.

    __Solution__: Parse to pandas dataframe, transform flat columns names to MultiIndex and rename sensors positions to readable names
    
    __Required__: unit tests
    
    - Modules _/data.py_, _/test/data_tests.py_
    
2. Implement functions for multiplication of quaternions and calculation of angle from them.
    
    __Required__: unit tests to those functions
    
    - Modules _/geometry.py_, _/test/geometry_tests.py_
      
3. Implement function with calculation of angles between required sensors and organizing of convenient dataframe for plotting.
   
    - Module _/calc.py_

4. Implement plotting function. Use library _plotly_ for plotting - very convenient and interactive plots

    - Module _/plot.py_

5. Run all functions and see to plot. Result graphic is under folder _/result_