import unittest

from pandas import Index

from data import index_to_multi_index


class DataTests(unittest.TestCase):
    def test_index_to_multi_index(self):
        columns = Index([
            'id',
            'q9_0.w', 'q9_0.x', 'q9_0.y', 'q9_0.z', 'q6_0.w', 'q6_0.x', 'q6_0.y', 'q6_0.z',
            'g_0.x', 'g_0.y', 'g_0.z', 'a_0.x', 'a_0.y', 'a_0.z',
            'm_0.x', 'm_0.y', 'm_0.z', 'la_0.x', 'la_0.y', 'la_0.z',
            'q9_1.w', 'q9_1.x', 'q9_1.y', 'q9_1.z', 'q6_1.w', 'q6_1.x', 'q6_1.y', 'q6_1.z',
            'g_1.x', 'g_1.y', 'g_1.z', 'a_1.x', 'a_1.y', 'a_1.z',
            'm_1.x', 'm_1.y', 'm_1.z', 'la_1.x', 'la_1.y', 'la_1.z',
            'q9_2.w', 'q9_2.x', 'q9_2.y', 'q9_2.z', 'q6_2.w', 'q6_2.x', 'q6_2.y', 'q6_2.z',
            'g_2.x', 'g_2.y', 'g_2.z', 'a_2.x', 'a_2.y', 'a_2.z',
            'm_2.x', 'm_2.y', 'm_2.z', 'la_2.x', 'la_2.y', 'la_2.z',
            'q9_3.w', 'q9_3.x', 'q9_3.y', 'q9_3.z', 'q6_3.w', 'q6_3.x', 'q6_3.y', 'q6_3.z',
            'g_3.x', 'g_3.y', 'g_3.z', 'a_3.x', 'a_3.y', 'a_3.z',
            'm_3.x', 'm_3.y', 'm_3.z', 'la_3.x', 'la_3.y', 'la_3.z',
            'q9_4.w', 'q9_4.x', 'q9_4.y', 'q9_4.z', 'q6_4.w', 'q6_4.x', 'q6_4.y', 'q6_4.z',
            'g_4.x', 'g_4.y', 'g_4.z', 'a_4.x', 'a_4.y', 'a_4.z',
            'm_4.x', 'm_4.y', 'm_4.z', 'la_4.x', 'la_4.y', 'la_4.z',
            'q9_5.w', 'q9_5.x', 'q9_5.y', 'q9_5.z', 'q6_5.w', 'q6_5.x', 'q6_5.y', 'q6_5.z',
            'g_5.x', 'g_5.y', 'g_5.z', 'a_5.x', 'a_5.y', 'a_5.z',
            'm_5.x', 'm_5.y', 'm_5.z', 'la_5.x', 'la_5.y', 'la_5.z',
            'q9_6.w', 'q9_6.x', 'q9_6.y', 'q9_6.z', 'q6_6.w', 'q6_6.x', 'q6_6.y', 'q6_6.z',
            'g_6.x', 'g_6.y', 'g_6.z', 'a_6.x', 'a_6.y', 'a_6.z',
            'm_6.x', 'm_6.y', 'm_6.z', 'la_6.x', 'la_6.y', 'la_6.z',
            'q9_7.w', 'q9_7.x', 'q9_7.y', 'q9_7.z', 'q6_7.w', 'q6_7.x', 'q6_7.y', 'q6_7.z',
            'g_7.x', 'g_7.y', 'g_7.z', 'a_7.x', 'a_7.y', 'a_7.z',
            'm_7.x', 'm_7.y', 'm_7.z', 'la_7.x', 'la_7.y', 'la_7.z',
            'q9_8.w', 'q9_8.x', 'q9_8.y', 'q9_8.z', 'q6_8.w', 'q6_8.x', 'q6_8.y', 'q6_8.z',
            'g_8.x', 'g_8.y', 'g_8.z', 'a_8.x', 'a_8.y', 'a_8.z',
            'm_8.x', 'm_8.y', 'm_8.z', 'la_8.x', 'la_8.y', 'la_8.z',
            'q9_9.w', 'q9_9.x', 'q9_9.y', 'q9_9.z', 'q6_9.w', 'q6_9.x', 'q6_9.y', 'q6_9.z',
            'g_9.x', 'g_9.y', 'g_9.z', 'a_9.x', 'a_9.y', 'a_9.z',
            'm_9.x', 'm_9.y', 'm_9.z', 'la_9.x', 'la_9.y', 'la_9.z',
            'q9_10.w', 'q9_10.x', 'q9_10.y', 'q9_10.z', 'q6_10.w', 'q6_10.x', 'q6_10.y', 'q6_10.z',
            'g_10.x', 'g_10.y', 'g_10.z', 'a_10.x', 'a_10.y', 'a_10.z',
            'm_10.x', 'm_10.y', 'm_10.z', 'la_10.x', 'la_10.y', 'la_10.z',
            'q9_11.w', 'q9_11.x', 'q9_11.y', 'q9_11.z', 'q6_11.w', 'q6_11.x', 'q6_11.y', 'q6_11.z',
            'g_11.x', 'g_11.y', 'g_11.z', 'a_11.x', 'a_11.y', 'a_11.z',
            'm_11.x', 'm_11.y', 'm_11.z', 'la_11.x', 'la_11.y', 'la_11.z',
            'q9_12.w', 'q9_12.x', 'q9_12.y', 'q9_12.z', 'q6_12.w', 'q6_12.x', 'q6_12.y', 'q6_12.z',
            'g_12.x', 'g_12.y', 'g_12.z', 'a_12.x', 'a_12.y', 'a_12.z',
            'm_12.x', 'm_12.y', 'm_12.z', 'la_12.x', 'la_12.y', 'la_12.z',
            'q9_13.w', 'q9_13.x', 'q9_13.y', 'q9_13.z', 'q6_13.w', 'q6_13.x', 'q6_13.y', 'q6_13.z',
            'g_13.x', 'g_13.y', 'g_13.z', 'a_13.x', 'a_13.y', 'a_13.z',
            'm_13.x', 'm_13.y', 'm_13.z', 'la_13.x', 'la_13.y', 'la_13.z',
            'timestamp', 'delta_time'
        ])
        multi_columns = index_to_multi_index(columns)
        self.assertEqual(len(columns), len(multi_columns))
        self.assertEqual(3, len(multi_columns.levels))
