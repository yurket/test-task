import unittest
import numpy as np

from pyquaternion import Quaternion

from geometry import apply_inverse_to, revert, angle

_HALF_SQRT_2 = np.sqrt(0.5)


class GeometryTests(unittest.TestCase):
    def test_quaternion_apply_inverse_to(self):
        quaternions1 = np.array([
            [1.0, 0.0, 0.0, 0.0],
            [_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [_HALF_SQRT_2, 0.0, _HALF_SQRT_2, 0.0],
            [0.0, _HALF_SQRT_2, _HALF_SQRT_2, 0.0]
        ])

        quaternions2 = np.array([
            [_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [1.0, 0.0, 0.0, 0.0],
            [0.0, _HALF_SQRT_2, _HALF_SQRT_2, 0.0],
            [_HALF_SQRT_2, 0.0, _HALF_SQRT_2, 0.0]
        ])

        expected_quaternions = np.array([
            [-_HALF_SQRT_2, 0.0, 0.0, -_HALF_SQRT_2],
            [-_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [-0.5, -0.5, -0.5, 0.5],
            [-0.5, 0.5, 0.5, -0.5]
        ])

        result = apply_inverse_to(quaternions1, quaternions2)
        self.assertEqual(result.shape, quaternions1.shape)
        np.testing.assert_array_almost_equal(expected_quaternions, result)

        for i in range(len(quaternions1)):
            result = apply_inverse_to(quaternions1[i], quaternions2[i])
            self.assertEqual((4,), result.shape)
            np.testing.assert_array_almost_equal(expected_quaternions[i], result)

    def test_custom_inverse_against_pyquaternion_division(self):
        q1 = Quaternion.random()
        q2 = Quaternion.random()
        res = -q2 / q1
        np.testing.assert_almost_equal(res.elements, apply_inverse_to(q1.elements, q2.elements))

    def test_quaternion_revert(self):
        quaternions = np.array([
            [1.0, 0.0, 0.0, 0.0],
            [_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [_HALF_SQRT_2, 0.0, _HALF_SQRT_2, 0.0],
            [0.0, _HALF_SQRT_2, _HALF_SQRT_2, 0.0]
        ])
        expected_quaternions = np.array([
            [-1.0, 0.0, 0.0, 0.0],
            [-_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [-_HALF_SQRT_2, 0.0, _HALF_SQRT_2, 0.0],
            [-0.0, _HALF_SQRT_2, _HALF_SQRT_2, 0.0]
        ])
        result = revert(quaternions)
        self.assertEqual(result.__hash__, quaternions.__hash__)
        np.testing.assert_array_equal(expected_quaternions, result)

    def test_angle_from_quaternion(self):
        quaternions = np.array([
            [1.0, 0.0, 0.0, 0.0],
            [_HALF_SQRT_2, 0.0, 0.0, _HALF_SQRT_2],
            [-_HALF_SQRT_2, 0.0, _HALF_SQRT_2, 0.0],
            [0.0, _HALF_SQRT_2, _HALF_SQRT_2, 0.0]
        ])
        expected_angles = np.array([0.0, np.pi / 2, np.pi / 2, np.pi])
        result = angle(quaternions)
        self.assertEqual(len(quaternions), len(result))
        np.testing.assert_array_almost_equal(expected_angles, result)
