from calc import get_angles_between_positions
from data import load_data
from plot import plot_angles

if __name__ == '__main__':
    df = load_data('/Users/iselivanov/Downloads/TestTaskMocapTeam_v0.2/raw_mocap-4.5.5-2020-11-11-15-22-01-relax_after_shuttle_run.csv')
    angles_df = get_angles_between_positions(df, 'right_shin', 'right_thigh')
    plot_angles(angles_df, '/Users/iselivanov/Downloads/TestTaskMocapTeam_v0.2/plot.html')
