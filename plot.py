from typing import Optional

from plotly import express as px
import pandas as pd


def plot_angles(angles: pd.DataFrame, filepath_to_save: Optional[str] = None):
    fig = px.line(angles, x='Time', y='Angle')
    fig.update_xaxes(title='Time, seconds')
    fig.update_yaxes(title='Angle, degrees')
    fig.show()
    if filepath_to_save is not None:
        fig.write_html(filepath_to_save)
