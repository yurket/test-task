import numpy as np


def apply_inverse_to(q1: np.array, q2: np.array) -> np.array:
    """
    Returns -q1 * inv(q2)
     :param q1 - quaternions array which are applied
     :param q2 - quaternions array which are to be applied

     :returns Applied quaternions array
    """

    assert q1.shape[0] == q2.shape[0], f'Inconsistent length of arrays: first is {len(q1)}, ' \
                                       f'other is {len(q2)}'
    if len(q1.shape) == 1:
        if q1.shape[0] == 4:
            return apply_inverse_to_one(q1, q2)
        else:
            raise RuntimeError(f'Wrong dimension for the quaternion: {q1.shape}')

    if q1.shape[0] == 1:
        return apply_inverse_to_one(q1[0], q2[0])
    else:
        result = np.zeros(q1.shape)
        result[:, 0] = -q1[:, 0] * q2[:, 0] - (q1[:, 1] * q2[:, 1] + q1[:, 2] * q2[:, 2] + q1[:, 3] * q2[:, 3])
        result[:, 1] = -q1[:, 0] * q2[:, 1] + q1[:, 1] * q2[:, 0] + (q1[:, 3] * q2[:, 2] - q1[:, 2] * q2[:, 3])
        result[:, 2] = -q1[:, 0] * q2[:, 2] + q1[:, 2] * q2[:, 0] + (q1[:, 1] * q2[:, 3] - q1[:, 3] * q2[:, 1])
        result[:, 3] = -q1[:, 0] * q2[:, 3] + q1[:, 3] * q2[:, 0] + (q1[:, 2] * q2[:, 1] - q1[:, 1] * q2[:, 2])
        return result


def apply_inverse_to_one(q1: np.array, q2: np.array) -> np.array:
    return np.array([
        -q1[0] * q2[0] - (q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3]),
        -q1[0] * q2[1] + q1[1] * q2[0] + (q1[3] * q2[2] - q1[2] * q2[3]),
        -q1[0] * q2[2] + q1[2] * q2[0] + (q1[1] * q2[3] - q1[3] * q2[1]),
        -q1[0] * q2[3] + q1[3] * q2[0] + (q1[2] * q2[1] - q1[1] * q2[2])
    ])


def revert(quaternion: np.array) -> np.array:
    """
    Returns -inv(q2)
     :param quaternion - quaternions array which are to be reverted

     :returns reverted quaternion as input array
    """
    quaternion[:, 0] *= -1
    return quaternion


def delta(q1: np.array, q2: np.array) -> np.array:
    return revert(apply_inverse_to(q1, q2))


def angle(quaternion: np.array) -> np.array:
    """
    Returns angles array for each quaternion in the array
    :param quaternion: input quaternions array
    :return: array of the angles in radians
    """
    result = np.zeros(len(quaternion))
    w_negative = quaternion[:, 0] < 0
    w_around_zero = (-0.1 <= quaternion[:, 0]) & (quaternion[:, 0] <= 0.1)
    result[~w_around_zero] = 2.0 * np.arcsin(np.linalg.norm(quaternion[~w_around_zero, 1:], axis=1))
    result[w_around_zero & w_negative] = 2.0 * np.arccos(-quaternion[w_around_zero & w_negative, 0])
    result[w_around_zero & ~w_negative] = 2.0 * np.arccos(quaternion[w_around_zero & ~w_negative, 0])
    return result
